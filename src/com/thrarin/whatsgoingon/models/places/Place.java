package com.thrarin.whatsgoingon.models.places;

import java.io.File;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.thrarin.whatsgoingon.listenner.AppServletContextListener;
import com.thrarin.whatsgoingon.models.Event;
import com.thrarin.whatsgoingon.utils.InternetConnection;

public abstract class Place {

    protected static final String EVENTS = "events";
    protected static final String EVENT = "event";
    protected static final String EVENT_DATE = "date";
    protected static final String EVENT_TITLE = "title";
    protected static final String EVENT_DESCRIPTION = "description";

    protected String mName;
    protected String mURL;
    protected String mLocation;
    protected String mXmlName;

    protected List<Event> sEvents = new ArrayList<>();
    protected String sDate = "";
    protected Document sDocument;

    public Place() {
        defineInfos();
    }

    protected abstract void defineInfos();

    protected abstract List<Event> parserHtml(org.jsoup.nodes.Document doc);

    protected boolean compareDates(String eventDate) {

        String currentDate = getCurrentDate();

        if (eventDate.contains(currentDate))
            return true;

        return false;
    }

    protected String getCurrentDate() {

        Calendar mCalendar = Calendar.getInstance();
        int dayOfTheMonth = mCalendar.get(Calendar.DAY_OF_MONTH);
        int month = mCalendar.get(Calendar.MONTH) + 1;

        DecimalFormat df = new DecimalFormat("00");
        String date = df.format(dayOfTheMonth) + "/" + df.format(month);

        return date;
    }

    protected Document getEventsDocument() {

        Document doc = null;

        File fXmlFile = new File(getXMLPlacePath());

        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder dBuilder;
        try {
            dBuilder = dbFactory.newDocumentBuilder();
            doc = dBuilder.parse(fXmlFile);

            doc.getDocumentElement().normalize();

        } catch (ParserConfigurationException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (SAXException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }

        return doc;
    }

    protected boolean saveEventsDocument(Document document) {

        TransformerFactory transformerFactory = TransformerFactory
                .newInstance();

        File file = null;

        try {

            Transformer transformer = transformerFactory.newTransformer();
            transformer.setOutputProperty(
                    "{http://xml.apache.org/xslt}indent-amount", "2");
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");

            DOMSource source = new DOMSource(document);

            file = new File(getXMLPlacePath() + ".xml");

            StreamResult result = new StreamResult(file);

            transformer.transform(source, result);

            return true;
        } catch (TransformerConfigurationException e) {
            e.printStackTrace();
        } catch (TransformerException e) {
            System.out.print(e.getMessage());

            file = new File(getXMLPlacePath() + ".xml");
            try {
                file.getParentFile().mkdirs();
                file.createNewFile();
            } catch (IOException e1) {
                System.out.print(e.getMessage());
                return false;
            }

            return saveEventsDocument(document);
        }

        return false;
    }

    protected boolean saveEvents(List<Event> eventsList) {

        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder dBuilder;

        try {

            dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.newDocument();

            Element rootElement = doc.createElement(EVENTS);
            doc.appendChild(rootElement);

            for (Event event : eventsList) {
                rootElement.appendChild(getEventNode(doc, event));
            }

            refreshEvents(eventsList);

            return saveEventsDocument(doc);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return false;
    }

    protected void refreshEvents(List<Event> eventsList) {
        sEvents = eventsList;
        sDate = getCurrentDate();
    }

    public List<Event> readEvents() {

        List<Event> mEvents = new ArrayList<Event>();

        Document document = getEventsDocument();

        if (document != null) {
            NodeList nodeList = document.getElementsByTagName(EVENT);
            int quantity = nodeList.getLength();

            for (int i = 0; i < quantity; i++) {

                Node node = nodeList.item(i);

                if (node.getNodeType() == Node.ELEMENT_NODE) {

                    Element eElement = (Element) node;

                    Event event = new Event();
                    event.setDate(eElement.getElementsByTagName(EVENT_DATE)
                            .item(0).getTextContent());
                    event.setTitle(eElement.getElementsByTagName(EVENT_TITLE)
                            .item(0).getTextContent());
                    event.setDescription(eElement
                            .getElementsByTagName(EVENT_DESCRIPTION).item(0)
                            .getTextContent());
                    event.setPlace(this.getName());
                    
                    mEvents.add(event);
                }
            }
        }

        return mEvents;
    }

    public List<Event> getEvents() {
        return sEvents;
    }

    public Event getTodayEvent() {

        for (Event event : sEvents) {

            if (compareDates(event.getDate())) {
                return event;
            }
        }

        return null;
    }

    public boolean isUpdated() {

        if (compareDates(sDate))
            return true;

        return false;
    }

    public boolean updateEvent() {

        org.jsoup.nodes.Document doc = getHTMLContent();

        if (doc != null) {

            List<Event> mEvent = parserHtml(doc);

            if (mEvent != null) {
                return saveEvents(mEvent);
            }
        }

        return false;
    }

    public boolean updateEventFromXMLFile() {

        List<Event> mEvents = readEvents();

        this.sEvents = mEvents;

        return true;
    }

    public String getURL() {
        return mURL;
    }

    public String getLocation() {
        return mLocation;
    }

    public String getName() {
        return mName;
    }

    public String getXmlName() {
        return mXmlName;
    }

    private Node getEventNode(Document doc, Event event) {

        Element node = doc.createElement(EVENT);

        // create age element
        node.appendChild(getEventElements(doc, node, EVENT_DATE,
                event.getDate()));

        // create role element
        node.appendChild(getEventElements(doc, node, EVENT_TITLE,
                event.getTitle()));

        // create gender element
        node.appendChild(getEventElements(doc, node, EVENT_DESCRIPTION,
                event.getDescription()));

        return node;
    }

    private String getXMLPlacePath() {
        return AppServletContextListener.getApplicationPath() + "Data"
                + System.getProperty("file.separator") + getXmlName() + ".xml";
    }

    // utility method to create text node
    private Node getEventElements(Document doc, Element element, String name,
            String value) {
        Element node = doc.createElement(name);
        node.appendChild(doc.createTextNode(value));
        return node;
    }

    private org.jsoup.nodes.Document getHTMLContent() {

        org.jsoup.nodes.Document mDocument = null;
        String url = getURL();

        // Check if event is already updated
        if (isUpdated())
            return null;

        if (url != null && !url.isEmpty()) {
            mDocument = InternetConnection.getURLContent(url);
        }

        return mDocument;
    }
}