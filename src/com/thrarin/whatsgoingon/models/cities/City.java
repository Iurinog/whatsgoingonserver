package com.thrarin.whatsgoingon.models.cities;

import java.util.Arrays;
import java.util.List;

import com.thrarin.whatsgoingon.models.places.Place;
import com.thrarin.whatsgoingon.utils.Utils.Category;

public abstract class City {

    protected static Place[] empty = {};

    public List<Place> getPlaces(Category category) {
        return Arrays.asList(getPlacesByCategory(category));
    }

    protected Place[] getPlacesByCategory(Category category) {

        switch (category) {
        case COUNTRY:
            return empty;
        case ELETRO:
            return empty;
        case PUB:
            return empty;
        case THEATER:
            return empty;
        case SHOW:
            return empty;

        default:
            return empty;
        }
    }

    public Place getPlace(Category category, String placeName) {

        Place[] places = getPlacesByCategory(category);

        for (Place place : places) {

            if (place.getName().equalsIgnoreCase(placeName))
                return place;
        }

        return null;
    }

    public List<Place> getAllPlacesInCategory(Category category) {
        return Arrays.asList(getPlacesByCategory(category));
    }

}
