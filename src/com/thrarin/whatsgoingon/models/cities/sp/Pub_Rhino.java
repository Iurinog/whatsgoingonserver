package com.thrarin.whatsgoingon.models.cities.sp;

import java.util.ArrayList;
import java.util.List;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import com.thrarin.whatsgoingon.models.Event;
import com.thrarin.whatsgoingon.models.places.Place;

public class Pub_Rhino extends Place {

    @Override
    protected void defineInfos() {
        mXmlName = "sp_rhino";
        mName = "Rhino Pub";
        mURL = "http://www.rhinopub.com.br/new/eventos-3/agenda-mensal";
        mLocation = "Av. Cotovia, 99 - Moema - S�o Paulo - (11) 5095-9770 / 5041-4188";
    }

    @Override
    protected List<Event> parserHtml(Document doc) {

        List<Event> events = new ArrayList<Event>();

        if (doc != null) {

            Elements mElements = doc.getElementsByClass("calendar-day");

            mElements = removeOutOfDateElements(mElements);

            for (int i = 0; i < mElements.size(); i++) {

                Element mElement = mElements.get(i);

                String eventDate = mElement.getElementsByClass("date").get(0)
                        .text();

                // if (compareDates(eventDate)) {
                if (true) {

                    String description = mElement
                            .getElementsByClass("show-time").get(0).text();

                    String title = mElement.getElementsByClass("show-info")
                            .get(0).text();

                    title = title.replace(description, "");

                    Event mEvent = new Event();
                    mEvent.setDate(eventDate);
                    mEvent.setTitle(title);
                    mEvent.setDescription(description);
                    mEvent.setPlace(this.mXmlName);

                    events.add(mEvent);
                }
            }

        }

        return events;
    }

    private Elements removeOutOfDateElements(Elements elements) {

        Elements mElements = new Elements();

        for (Element element : elements) {

            if (!element.toString().contains("out_of_range")) {
                mElements.add(element);
            }
        }

        return mElements;
    }

}
