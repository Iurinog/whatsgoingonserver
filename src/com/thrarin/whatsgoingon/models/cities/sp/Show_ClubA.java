package com.thrarin.whatsgoingon.models.cities.sp;

import java.util.ArrayList;
import java.util.List;

import org.jsoup.nodes.Document;

import com.thrarin.whatsgoingon.models.Event;
import com.thrarin.whatsgoingon.models.places.Place;

public class Show_ClubA extends Place {

    @Override
    protected void defineInfos() {
        mXmlName = "sp_cluba";
        mName = "Club A";
        mURL = "http://www.clubasaopaulo.com.br/v2/programacao.php";
        mLocation = "Av. das Na��es Unidas, 12.559, piso C (acesso pelo lobby do Sheraton S�o Paulo WTC Hotel) - Cidade Mon��es - S�o Paulo - (11) 3043-8343 ";
    }

    @Override
    protected List<Event> parserHtml(Document doc) {
        return new ArrayList<Event>();
    }

}
