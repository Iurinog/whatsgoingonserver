package com.thrarin.whatsgoingon.models.cities.sp;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import com.thrarin.whatsgoingon.models.Event;
import com.thrarin.whatsgoingon.models.places.Place;

public class Country_VillaCountry extends Place {

    @Override
    protected void defineInfos() {
        mXmlName = "sp_villacountry";
        mName = "Villa Country";
        mURL = "http://www.villacountry.com.br/villa/Agenda_int.asp?c_data="
                + getVilaCountryDate();
        mLocation = "Av. Francisco Matarazzo, 774 - �gua Branca - S�o Paulo - (11) 3868-5858";
    }

    @Override
    protected List<Event> parserHtml(Document doc) {

        List<Event> events = new ArrayList<Event>();

        if (doc != null) {

            Elements mElements = doc.getElementsByTag("AgendaMeio");

            for (int i = 0; i < mElements.size(); i++) {

                Element mElement = mElements.get(i);

                String eventDate = mElement.getElementsByClass("text_11")
                        .get(0).text();
                String description = mElement.getElementsByClass("text_11")
                        .get(1).text();

                String title = mElement.getElementsByClass("text_12").get(0)
                        .text();

                // if (compareDates(eventDate)) {
                if (true) {

                    Event mEvent = new Event();
                    mEvent.setDate(eventDate);
                    mEvent.setTitle(title);
                    mEvent.setDescription(description);
                    mEvent.setPlace(this.mXmlName);

                    events.add(mEvent);
                }

            } // for - elements
        }

        return events;
    }

    private String getVilaCountryDate() {

        Calendar mCalendar = Calendar.getInstance();
        int dayOfTheMonth = mCalendar.get(Calendar.DAY_OF_MONTH);
        int month = mCalendar.get(Calendar.MONTH) + 1;
        int year = mCalendar.get(Calendar.YEAR);

        DecimalFormat df = new DecimalFormat("00");

        return "" + year + df.format(month) + dayOfTheMonth;
    }

}
