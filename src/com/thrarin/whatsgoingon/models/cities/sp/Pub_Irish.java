package com.thrarin.whatsgoingon.models.cities.sp;

import java.util.ArrayList;
import java.util.List;

import org.jsoup.nodes.Document;

import com.thrarin.whatsgoingon.models.Event;
import com.thrarin.whatsgoingon.models.places.Place;

public class Pub_Irish extends Place {

    @Override
    protected void defineInfos() {
        mXmlName = "sp_irishpub";
        mName = "Irish Pub";
    }

    @Override
    protected List<Event> parserHtml(Document doc) {
        return new ArrayList<Event>();
    }

}
