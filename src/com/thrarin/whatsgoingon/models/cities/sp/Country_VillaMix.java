package com.thrarin.whatsgoingon.models.cities.sp;

import java.util.ArrayList;
import java.util.List;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import com.thrarin.whatsgoingon.models.Event;
import com.thrarin.whatsgoingon.models.places.Place;

public class Country_VillaMix extends Place {

    @Override
    protected void defineInfos() {
        mXmlName = "sp_villamix";
        mName = "Villa Mix";
        mURL = "http://www.villamixsp.com.br/programacao.php";
        mLocation = "Rua Beira Rio, 116 - Vila Ol�mpia - S�o Paulo - (11) 3044-3241";
    }

    @Override
    protected List<Event> parserHtml(Document doc) {

        List<Event> events = new ArrayList<Event>();

        if (doc != null) {

            Elements mElements = doc.getElementsByClass("caixa_textocor_g1");

            for (int i = 0; i < mElements.size(); i++) {

                Element mElement = mElements.get(i);

                String eventDate = mElement.getElementsByClass("text_11")
                        .get(0).text();
                String description = mElement.getElementsByClass("text_11")
                        .get(1).text();

                String title = mElement.getElementsByClass("text_12").get(0)
                        .text();

                // if (compareDates(eventDate)) {
                if (true) {

                    Event mEvent = new Event();
                    mEvent.setDate(eventDate);
                    mEvent.setTitle(title);
                    mEvent.setDescription(description);
                    mEvent.setPlace(this.mXmlName);
                    
                    events.add(mEvent);
                }
            } // for - elements
        }

        return events;
    }

}
