package com.thrarin.whatsgoingon.models.cities;

import com.thrarin.whatsgoingon.models.cities.sp.Country_VillaCountry;
import com.thrarin.whatsgoingon.models.cities.sp.Country_VillaMix;
import com.thrarin.whatsgoingon.models.cities.sp.Eletro_LeReveClub;
import com.thrarin.whatsgoingon.models.cities.sp.Pub_Irish;
import com.thrarin.whatsgoingon.models.cities.sp.Pub_Rhino;
import com.thrarin.whatsgoingon.models.cities.sp.Show_ClubA;
import com.thrarin.whatsgoingon.models.places.Place;
import com.thrarin.whatsgoingon.utils.Utils.Category;

public class SaoPaulo extends City {

    private static Place[] country = { new Country_VillaMix(),
            new Country_VillaCountry() };

    private static Place[] eletro = { new Eletro_LeReveClub() };
    private static Place[] pub = { new Pub_Rhino(), new Pub_Irish() };
    private static Place[] show = { new Show_ClubA() };

    public SaoPaulo() {
    }

    @Override
    protected Place[] getPlacesByCategory(Category category) {

        switch (category) {
        case COUNTRY:
            return country;
        case ELETRO:
            return eletro;
        case PUB:
            return pub;
        case THEATER:
            return empty;
        case SHOW:
            return show;

        default:
            return null;
        }
    }
}
