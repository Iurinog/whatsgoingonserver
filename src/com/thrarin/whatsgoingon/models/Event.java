package com.thrarin.whatsgoingon.models;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Event {

    private String mPlace;
    private String mTitle;
    private String mDescription;
    private String mDate;

    public String getPlace() {
        return mPlace;
    }

    public void setPlace(String mPlace) {
        this.mPlace = mPlace;
    }

    public String getDate() {
        return mDate;
    }

    public void setDate(String mDate) {
        this.mDate = mDate;
    }

    public String getTitle() {
        return mTitle;
    }

    public void setTitle(String mTitle) {
        this.mTitle = mTitle;
    }

    public String getDescription() {
        return mDescription;
    }

    public void setDescription(String mDescription) {
        this.mDescription = mDescription;
    }

}
