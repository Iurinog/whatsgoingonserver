package com.thrarin.whatsgoingon.listenner;

import java.io.File;
import java.util.Timer;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import com.thrarin.whatsgoingon.utils.UpdateEventsTimerTask;

public class AppServletContextListener implements ServletContextListener {

    // private static final long INTERVAL_DURATION = 24 * 60 * 60 * 1000;
    private static final long INTERVAL_DURATION = 24 * 60 * 1000;
    private UpdateEventsTimerTask myTimerTask;

    private static String contextPath = "";

    @Override
    public void contextDestroyed(ServletContextEvent arg0) {
        System.out.println("ServletContextListener WhatsGoingOn destroyed");
        System.out.println("Iuriiii --- Listener shutted down!");
    }

    @Override
    public void contextInitialized(ServletContextEvent arg0) {
        System.out.println("ServletContextListener WhatsGoingOn started");
        System.out.println("Iuriiii --- Listener Working!");
        
        contextPath = arg0.getServletContext().getRealPath(File.separator);
        
        myTimerTask = new UpdateEventsTimerTask();

        Timer timer = new Timer(true);
        timer.scheduleAtFixedRate(myTimerTask, 0, INTERVAL_DURATION);
    }

    public static String getApplicationPath() {
        return contextPath;
    }

}
