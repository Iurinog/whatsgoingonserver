package com.thrarin.whatsgoingon.resources;

import java.util.List;

import javax.ejb.EJB;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

import com.thrarin.whatsgoingon.controller.EventsManager;
import com.thrarin.whatsgoingon.models.Event;
import com.thrarin.whatsgoingon.models.places.Place;

@Path("/events")
public class EventResource {

    @EJB
    private EventsManager eventsManager;

    @GET
    @Path("/random")
    @Produces("text/xml")
    public Event getRandomEvent() {

        System.out.println("");
        System.out.println("EventResource - Get Randon Event");

        eventsManager = new EventsManager();
        return eventsManager.getRandomEvent();
    }

    @GET
    @Path("/all")
    @Produces("text/xml")
    public List<Event> getAllEvents() {

        System.out.println("");
        System.out.println("EventResource - Get All Events");

        eventsManager = new EventsManager();
        return eventsManager.getAllEvents();
    }

    @GET
    @Path("/event/{city}/{category}/{place}")
    @Produces("text/xml")
    public List<Event> getEventsByPlace(@PathParam("city") String city,
            @PathParam("category") String category,
            @PathParam("place") String place) {

        System.out.println("");
        System.out.println("EventResource - Get Specific Event");
        System.out.println("Event - City: " + city + " - Category: " + category
                + " - Place: " + place);

        eventsManager = new EventsManager();
        return eventsManager.getEvents(city, category, place);
    }

    @GET
    @Path("/event/{city}/{category}")
    @Produces("text/xml")
    public List<Event> getEventsByCategory(@PathParam("city") String city,
            @PathParam("category") String category) {

        System.out.println("");
        System.out.println("EventResource - Get Events by Category");
        System.out
                .println("Event - City: " + city + " - Category: " + category);

        eventsManager = new EventsManager();
        return eventsManager.getEvents(city, category);
    }

    @GET
    @Path("/places/all")
    @Produces("text/xml")
    public List<Place> getAllPlace() {

        System.out.println("");
        System.out.println("EventResource - Get All Events");

        eventsManager = new EventsManager();
        return eventsManager.getAllPlaces();
    }
}
