package com.thrarin.whatsgoingon.utils;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

public class InternetConnection {

	public static Document getURLContent(String link) {

		try {
			URL mURL = getURLFromString(link);
			URLConnection mURLConnection = mURL.openConnection();

			if (mURLConnection != null) {

				Document mDocument = Jsoup.parse(
						mURLConnection.getInputStream(), null, link);

				if (mDocument != null) {
					return mDocument;
				}
			}

		} catch (MalformedURLException e1) {
			e1.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return null;
	}

	private static URL getURLFromString(String link)
			throws MalformedURLException {
		return new URL(link);
	}
}
