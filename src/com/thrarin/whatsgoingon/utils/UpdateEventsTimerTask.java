package com.thrarin.whatsgoingon.utils;

import java.util.TimerTask;

import javax.ejb.EJB;

import com.thrarin.whatsgoingon.controller.Controller;

public class UpdateEventsTimerTask extends TimerTask {

    @EJB
    private Controller controller;

    public UpdateEventsTimerTask() {
        controller = new Controller();
    }

    @Override
    public void run() {

        try {
            Thread.sleep(10000);
        } catch (InterruptedException e) {
            System.out.println(e.getMessage());
        }

        controller.updateEvents();
    }
}