package com.thrarin.whatsgoingon.utils;

public class Utils {
    
    public static enum Cities {
        SaoPaulo, RioDeJaneiro, BeloHorizonte
    }

    public static enum Category {
        COUNTRY, ELETRO, PUB, THEATER, SHOW
    }
}
