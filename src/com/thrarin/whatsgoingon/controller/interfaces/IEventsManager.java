package com.thrarin.whatsgoingon.controller.interfaces;

import java.util.List;

import com.thrarin.whatsgoingon.models.Event;
import com.thrarin.whatsgoingon.models.places.Place;

public interface IEventsManager {

	/**
	 * Used for test
	 */
	public Event getRandomEvent();

	public Place getPlace(String cityName, String categoryName, String placeName);

	public List<Place> getPlaces(String city, String category);

	public List<Place> getAllPlaces();

	public List<Event> getEvents(String cityName, String categoryName,
			String placeName);
	
	public List<Event> getEvents(String cityName, String categoryName);
	
	public List<Event> getAllEvents();
}
