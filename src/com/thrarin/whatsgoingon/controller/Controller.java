package com.thrarin.whatsgoingon.controller;

import java.util.List;

import javax.ejb.Stateless;

import com.thrarin.whatsgoingon.controller.interfaces.IController;
import com.thrarin.whatsgoingon.models.places.Place;

@Stateless
public class Controller implements IController {

	private EventsManager eventsManager;

	/**
	 * Constructor
	 */
	public Controller() {
		eventsManager = new EventsManager();
	}

	@Override
	public void updateEvents() {
		
		System.out.println("Updating Events");

        List<Place> places = eventsManager.getAllPlaces();

        for (Place place : places) {
            System.out.println("Updating Event: " + place.getName());
           place.updateEventFromXMLFile();
        }
        
        System.out.println("Updating Events Complete!");
	}

}
