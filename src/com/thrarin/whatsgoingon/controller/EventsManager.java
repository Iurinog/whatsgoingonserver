package com.thrarin.whatsgoingon.controller;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;

import com.thrarin.whatsgoingon.controller.interfaces.IEventsManager;
import com.thrarin.whatsgoingon.models.Event;
import com.thrarin.whatsgoingon.models.cities.City;
import com.thrarin.whatsgoingon.models.cities.SaoPaulo;
import com.thrarin.whatsgoingon.models.places.Place;
import com.thrarin.whatsgoingon.utils.Utils.Category;
import com.thrarin.whatsgoingon.utils.Utils.Cities;

@Stateless
public class EventsManager implements IEventsManager {

    @Override
    public Event getRandomEvent() {

        Event event = new Event();
        event.setTitle("Event Name");
        event.setDescription("Event Description");
        event.setDate("Today");

        return event;
    }

    @Override
    public Place getPlace(String cityName, String categoryName, String placeName) {

        Place place = null;

        City city = getCity(cityName);
        Category category = getCategory(categoryName);

        if (city != null && category != null) {
            return city.getPlace(category, placeName);
        }

        return place;
    }

    @Override
    public List<Place> getPlaces(String city, String category) {

        List<Place> mPlaces = new ArrayList<Place>();

        City mCity = getCity(city);
        Category mCategory = getCategory(category);

        if (mCity != null && mCategory != null) {
            List<Place> places = mCity.getPlaces(mCategory);
            mPlaces.addAll(places);
        }

        return mPlaces;
    }

    @Override
    public List<Place> getAllPlaces() {

        List<Place> places = new ArrayList<Place>();

        // Get all Cities
        for (Cities cityName : Cities.values()) {

            // Get all Categories
            for (Category categoryName : Category.values()) {

                City city = getCity(cityName.name());
                Category category = getCategory(categoryName.name());

                if (city != null && category != null) {
                    places.addAll(city.getPlaces(category));
                }
            }
        }

        return places;
    }

    @Override
    public List<Event> getEvents(String cityName, String categoryName,
            String placeName) {

        City city = getCity(cityName);
        Category category = getCategory(categoryName);

        if (city != null && category != null) {
            Place place = city.getPlace(category, placeName);

            if (place != null) {
                return place.getEvents();
            }
        }

        return new ArrayList<Event>();
    }

    @Override
    public List<Event> getEvents(String cityName, String categoryName) {

        List<Event> events = new ArrayList<>();

        City city = getCity(cityName);
        Category category = getCategory(categoryName);

        if (city != null && category != null) {

            List<Place> places = city.getAllPlacesInCategory(category);

            for (Place place : places) {

                Event event = place.getTodayEvent();

                if (event != null) {
                    events.add(event);
                }
            }
        }

        return events;
    }

    @Override
    public List<Event> getAllEvents() {

        List<Place> places = getAllPlaces();
        List<Event> allEvents = new ArrayList<>();

        for (Place place : places) {
            allEvents.addAll(place.getEvents());
        }

        return allEvents;
    }

    private City getCity(String city) {

        if (city.equalsIgnoreCase(Cities.SaoPaulo.name())) {
            return new SaoPaulo();
        } else if (city.equalsIgnoreCase(Cities.RioDeJaneiro.name())) {
            return null;
        } else if (city.equalsIgnoreCase(Cities.BeloHorizonte.name())) {
            return null;
        }

        return null;
    }

    private Category getCategory(String category) {

        if (category.equalsIgnoreCase(Category.COUNTRY.name())) {
            return Category.COUNTRY;
        } else if (category.equalsIgnoreCase(Category.ELETRO.name())) {
            return Category.ELETRO;
        } else if (category.equalsIgnoreCase(Category.PUB.name())) {
            return Category.PUB;
        } else if (category.equalsIgnoreCase(Category.SHOW.name())) {
            return Category.SHOW;
        } else if (category.equalsIgnoreCase(Category.THEATER.name())) {
            return Category.THEATER;
        }

        return null;
    }

}
